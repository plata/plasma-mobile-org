---
title: Gear Releases
menu:
  main:
    parent: contributing
    weight: 5
--- 

This is the list of Plasma Mobile Gear releases with tarball links, which is specifically for Plasma Mobile **applications**.

Plasma Mobile shell releases are alongside the regular Plasma releases.

See the [release schedule page](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Release-Schedule) for more details.
