---
title: Screenshots
layout: page
screenshots:
  - url: /screenshots/plasma.png
    name: "Plasma mobile homescreen"
  - url: /screenshots/weather.png
    name: KWeather, Plasma mobile weather application
  - url: /screenshots/pp_calculator.png
    name: Kalk, a calculator application
  - url: /screenshots/pp_camera.png
    name: Megapixels, a camera application
  - url: /screenshots/pp_calindori.png
    name: Calindori, a calendar application
  - url: /screenshots/pp_kclock.png
    name: KClock
  - url: /screenshots/pp_buho.png
    name: Buho, a note taking application
  - url: /screenshots/pp_kongress.png
    name: Kongress
  - url: /screenshots/pp_okular01.png
    name: Okular Mobile, a universal document viewer
  - url: /screenshots/pp_angelfish.png
    name: Angelfish, a web browser
  - url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
    name: Nota, a text editor
  - url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
    name: Pix, another image viewer
  - url: /screenshots/pp_folders.png
    name: Index, a file manager
  - url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
    name: VVave, a music player
  - url: /screenshots/20201110_092718.jpg
    name: The hardware
menu:
  main:
    parent: project
    weight: 2
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
---

The following screenshots were taken from a Pinephone device running Plasma Mobile.

{{< screenshots name="screenshots" >}}
