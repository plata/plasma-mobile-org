---
title: "Plasma Mobile: More Applications and an Improved Homescreen"
subtitle: Plasma Mobie in March and April 2021
SPDX-License-Identifier: CC-BY-4.0
date: 2021-04-27
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
- SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
- SPDX-FileCopyrightText: 2021 Ilya Bizyaev <bizyaev@zoho.com>
kclock:
  - name: Drawer
    url: kclock-form.png
  - name: Alarms
    url: kclock-mobile-alarms.png
  - name: Stopwatch
    url: kclock-mobile-stopwatch.png
  - name: Timers
    url: kclock-mobile-timers.png
  - name: Timezones
    url: kclock-mobile-timezones.png
krecorder:
  - name: Main view
    url: krecorder-list.png
  - name: Recording screen
    url: krecorder-record.png
  - name: Settings
    url: krecorder-settings.png
top-panel:
  - name: Pinned
    url: top-panel-pinned.png
  - name: Fully Open
    url: top-panel-open.png
alligator:
  - name: Alligator now playing
    url: alligator1.png
  - name: Alligator Queue
    url: alligator2.png
  - name: Alligator subscriptions
    url: alligator3.png
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
---

The Plasma Mobile team is happy to present the Plasma Mobile updates for March and April 2021. During the last two months, most of the work has been focused on applications. While we continued to improve existing applications making them more stable and featureful, we also worked on new applications, extending the Plasma Mobile app ecosystem. We also made noticeable improvements to the Plasma Mobile shell.

But, before we get into that, we would like to thank Pine64 for their generous donations for every KDE community edition device sold, and their support to our project. We would also like to extend our thanks to you, the owners of PinePhone KDE community edition devices: Your purchase helps us so much.

Aleix Pol i Gonzàlez, President of KDE e.V. says,

> Our collaboration with Pine64 is very important for two big reasons: we get a whole new hardware platform that respects our ways to create software and a new organization with a new approach towards hardware production and its integration with the FOSS communities to deliver a great user experience. We are very happy with the work so far and looking forward to more.

Lukasz Erecinski, Pine64 Community Manager,

> PINE64 has a long-standing friendship with the KDE Community and KDE e.V. Our cooperation goes all the way back to the days of the original Pinebook, launched in 2016, which shipped with KDE Neon. Currently, the Pinebook Pro and the PinePhone, our flagship devices, both ship with Plasma Desktop and Plasma Mobile respectively. It is fair to say that KDE was and is fundamental to our community’s growth and continued success.

On to the applications and other new things:

{{< section app_img_shadow="sm" app_img_alt="NeoChat on mobile" app_img="neochat-mobile.png" app_name="NeoChat" >}}

NeoChat switched to a brand new look and is now using bubbles for messages in the chat. Aside from that, Carl made some mobile-oriented improvements. The context menu, when long pressing on a message, now appears as a bottom drawer; and scrolling with your finger in the chat views is now more reliable. He also worked with Noah Davis to completely rewrite the chat input component, making it sleeker and adding transition animations.

Srevin Saju improved the keyboard navigation, making it easy to edit the last message sent by pressing <kbd>up</kbd>, among other things. Tobias Fella added the possibility of sending special text reactions by using the `/react <text>` command. He also improved the notification handling, refactored parts of the NeoChat codebase, and fixed many bugs and visual inconsistencies. He also started working on supporting the Matrix one-to-one audio chat feature and screen sharing with PipeWire.

{{< /section >}}

{{< section app_img_shadow="sm" app_img_alt="Elisa album view on mobile" direction="flex-sm-row-reverse" app_img="elisa.png" app_name="Elisa" >}}

The mobile version of Elisa by Devin Lin announced previously was merged, and is part of the newly released KDE Gear 21.04. Due to the improved mobile version, Mathieu Gallien worked on an Android version using the Android music indexer for discovering the music files. You can download an APK (in beta) from the [binary factory](https://binary-factory.kde.org/job/Elisa_android/).

{{< /section >}}

{{< section app_img_shadow="sm" app_img_alt="Koko new improved sidebar" app_img="koko.png" app_name="Koko" >}}

Mikel Johnson and Carl Schwan worked on many improvements to Koko. You can now see the image metadata from Koko and even modify some of the fields. A small image editor was added (using the same library as NeoChat). The folder view was completely rewritten and now allows for navigation on all folders on the phone, not just the Pictures folder. To make it easier to manage a large number of images, Mikel also added a tagging feature allowing for image tagging and filtering of pictures by tag. He also added a favorite collection.

{{< /section >}}

{{< section direction="flex-sm-row-reverse" app_img_alt="Gestures in Angelfish" app_img="gesture.gif" app_name="Angelfish" >}}

Devin Lin added a handy swipe feature to the navigation bar. You can now swipe left and right on it to go back and forward a page.

He also switched the tab page into a drawer, and added a swipe left/right gesture to close tabs.

Jonah Brüchert improved the performance of Angelfish by enabling hardware acceleration in QtWebEngine. This was disabled previously due to stability concerns, but it is now resolved with Qt 5.15.2.

{{< /section >}}

{{< section app_img_alt="Screenshot of AudioTube" app_img="https://jbbgameich.github.io/img/audiotube_mobile_playlist.png" app_name="AudioTube" >}}

Jonah Brüchert also worked on AudioTube, a new application to listen to YouTube Music.
The application is relatively new but is already in a usable state.
You can read more about it on [his blog post](https://jbbgameich.github.io/kde/2021/03/13/audiotube.html).

![AudioTube on the desktop](https://jbbgameich.github.io/img/audiotube_desktop.png)

{{< /section >}}

## Plasmatube

Plasmatube, a YouTube video player, also saw some improvements. Carl ported it away from mpv to QtMultimedia and added video controls. Linus Jan did significant refactoring, moving the Invidious API handling to an internal library and made it possible to use other Invidious instances.

![PlasmaTube on the desktop](plasmatube.png)

## DayKountdown

Clau Cambra worked on a new application that allows managing countdowns for special dates. This is the app that served as the inspiration to the new Kirigami tutorial. You can find it at https://invent.kde.org/plasma-mobile/daykountdown

![DayKountdown on the desktop](daykountdown.png)

{{< section app_img_shadow="sm" app_img_alt="Screenshot of OptiImage" app_img="optiimage.png" app_name="OptiImage" >}}

Carl Schwan started to work on an image optimizer based on _optipng_. In the future, other
image optimizers should be supported too.

{{< /section >}}

## Kolibri, a mail client

Carl Schwan and Simon Schmeißer started working on an email client using Kirigami. For now, it only allows reading mails, but an email composer is already in progress.

![Kollibri mail viewer](kolibri.png)

## KeePass Client

Tobias Fella started working on a KeePass compatible password manager. Currently, it is read only.

![KeePass password manager](keepass.png)

## KRecorder

Devin Lin started work on rewriting KRecorder's interface.

He redesigned the recording screen and list, and drawers are now used for easier one-handed usage.

{{< screenshots name="krecorder" >}}

## KClock

KClock has had a visual overhaul.

Devin Lin switched most of the forms to be drawers on mobile, added keyboard controls to the timer and stopwatch, and also refined the font weights, color and spacing throughout the application.

{{< screenshots name="kclock" >}}

## Alligator

Tobias Fella and Bart de Vries are working on adding podcast support to Alligator.

{{< screenshots name="alligator" >}}

## Shell

Devin Lin and Noah Davis were able to locate and fix a longstanding bug in Plasma where the default font does not support light or bold weights. The Plasma Mobile shell and applications are now able to make use of other font weights as it now works without needing to change fonts.

Marco Martin added an initial two-stage notification/quick settings drawer to the top of the shell. Devin further refined the behavior and appearance of the panel, while also refactoring the code.

The lockscreen was improved to be more performant with large amounts of notifications, and now also has more animations for keypad text. Work is also under way to allow the virtual keyboard to be used instead, so that text passwords can be used for logging in.

Marco Martin has also started working on multiple pages on the homescreen.

{{< screenshots name="top-panel" >}}

## Documentation

To make it easier to get started developing your own Plasma Mobile application, Clau Cambra continued to improve the Kirigami documentation, adding a lot of [small tutorials about how to use some components](https://develop.kde.org/docs/kirigami/). Carl Schwan wrote a more advanced tutorial about how to use [Akonadi to build a mail reader](https://develop.kde.org/docs/akonadi/using_akonadi_applications/). 

## Website

Carl did some housekeeping and ported the Jekyll website to Hugo, making translation easier. He also added a more complete list of all Plasma Mobile applications available on the homepage.
