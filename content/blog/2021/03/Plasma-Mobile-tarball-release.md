---
SPDX-License-Identifier: CC-BY-4.0
author: Bhushan Shah
authors:
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
created_at: 2021-03-04 17:30:00 UTC
date: "2021-03-04T00:00:00Z"
title: 'Plasma Mobile tarball release: bugfixes and new releases'
---

This release includes three new modules,

- krecorder
- qmlkonsole
- kirigami-addons

kirigami-addons is released as a unstable module and does not provide any API or ABI gurantees in this release

This release also includes the following bugfixes,

## plasma-dialer:

- Properly unset the suspend inhibition so that after call system is not stuck in awake state.

## spacebar:

- Fix icon rendering in the QtSVG that caused ugly look in Plasma Mobile
- Fix phone number normalization so that messages where sender address have alphabetical characters can be opened
- Add spacing in search bar to be consistent with other applications

Below are the details for this release items,

- plasma-dialer: [https://download.kde.org/stable/plasma-dialer/0.3/](https://download.kde.org/stable/plasma-dialer/0.3/)
```
b2c6f03ebcffce2145297fef7a4c74b9ede5503737e422b73c4b8e56886ac711  plasma-dialer-0.3.tar.xz.sig
1f79de22d21e4ad2979a862ca42174cde77ad695d52f24ad063969cc6877ffb3  plasma-dialer-0.3.tar.xz
```

- spacebar: [http://download.kde.org/stable/spacebar/0.2/](http://download.kde.org/stable/spacebar/0.2/)
```
a9df96b785dee11a0848cc1b0cebdeb9d2280f5ac99e0803bc006ea1c496980d  spacebar-0.2.tar.xz.sig
cbb7cbcef26cae82375816bf9203745e3990f4b7e837fd42f680b303b1a9ab4c  spacebar-0.2.tar.xz
```

- krecorder: [https://download.kde.org/unstable/krecorder/0.1/](https://download.kde.org/unstable/krecorder/0.1/)
```
a6e5c595b5056368c5fceafc17325dfeaa523c122cffe3500a98a55fcfa70359  krecorder-0.1.tar.xz.sig
74a6ac2601536feb42893ef4fd3a62cf6b9d9ee7562ba5785257a25d8f21a44a  krecorder-0.1.tar.xz
```

- qmlkonsole: [https://download.kde.org/unstable/qmlkonsole/0.1/](https://download.kde.org/unstable/qmlkonsole/0.1/)
```
e90af7351ea7b034bd056e76fb40d6429a00456aeed38feb510f9200289edb3b  qmlkonsole-0.1.tar.xz.sig
42b0a46c777e5d24e8a88c2605ba7b85eb1600026f2b5858392cfe8f548520dc  qmlkonsole-0.1.tar.xz
```

- kirigami-addons: [https://download.kde.org/unstable/kirigami-addons/0.1/](https://download.kde.org/unstable/kirigami-addons/0.1/)
```
ffbbde3e45a740ba3ed7bdec66b2351ad3379946826943501a048201ce692863  kirigami-addons-0.1.tar.xz.sig
b63f070ea62dd92670170aa885a6c1d478a4e9a42c1e00bf283882dadc81dd0a  kirigami-addons-0.1.tar.xz
```

All of the tarballs are signed with my PGP key,

0AAC775BB6437A8D9AF7A3ACFE0784117FBCE11D bshah at mykolab dot com

Happy packaging!
