---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Społeczność
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Grupy i kanały społeczności Przenośnej Plazmy:

### Kanały przeznaczone dla Przenośnej Plazmy:

* [![](/img/matrix.svg)Matrix (największy ruch)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Lista rozmów nt. Przenośnej Plazmy](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Kanały związane z Przenośną Plazmą:

* [![](/img/mail.svg)Lista rozmów nt. rozwoju Plazmy](https://mail.kde.org/mailman/listinfo/plasma-devel)
