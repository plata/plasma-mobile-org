---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Wizja
    parent: project
    weight: 1
title: Nasza wizja
---
Przenośna Plazma ma za zadania stać się pełnym i otwartym systemem oprogramowania dla urządzeń przenośnych.<br />Została opracowana z zamiarem zwrócenia władzy nad swoimi danymi i porozumiewaniem się użytkownikom, którzy są świadomi prywatności.

Przenośna Plazma podchodzi do rzeczy pragmatycznie i jest otwarta na oprogramowanie stron trzecich, umożliwiając użytkownikowi wybór między używanymi aplikacjami i usługami, zapewniając tym samym zwarte doświadczenie na wielu urządzeniach.<br />Przenośna Plazma implementuje otwarte standardy i jest rozwijana w przejrzysty sposób, który jest otwarty na współtworzenie dla każdego.
