---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Ekran domowy Przenośnej Plazmy
  url: /screenshots/plasma.png
- name: KWeather, aplikacja pogodowa dla Przenośnej Plazmy
  url: /screenshots/weather.png
- name: Kalk, aplikacja kalkulatora
  url: /screenshots/pp_calculator.png
- name: Megapixels, aplikacja aparatu
  url: /screenshots/pp_camera.png
- name: Calindori, aplikacja kalendarza
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, aplikacja do zapisków
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Przenośny Okular, wszechstronna przeglądarka dokumentów
  url: /screenshots/pp_okular01.png
- name: Angelfish, przeglądarka sieciowa
  url: /screenshots/pp_angelfish.png
- name: Nota, edytor tekstu
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, kolejna przeglądarka obrazów
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, przeglądarka plików
  url: /screenshots/pp_folders.png
- name: VVave, odtwarzacz muzyki
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Sprzęt
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Zrzuty ekranu
---
Następujące zrzuty ekranu zostały wykonane na urządzeniu Pinephone z uruchomioną Przenośną Plazmą.

{{< screenshots name="screenshots" >}}
