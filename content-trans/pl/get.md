---
layout: get-involved
menu:
  main:
    name: Wgraj
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Dystrybucje dostarczające Przenośną Plazmę
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Przenośna

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM jest dystrybucją Manjaro, lecz dla urządzeń ARM. Jest oparta na Arch Linux ARM, w połączeniu z narzędziami Manjaro, wyglądem i infrastrukturą, aby móc wgrywać obrazy na twoje urządzenia ARM.

[Strona sieciowa](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Pobierz

* [Najnowsze stabilne (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Wydania rozwojowe (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Wgrywanie

Ogólne dane nt. PinePhone możesz znaleźć na [wiki Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), jest urządzeniem dostosowanym do dotyku z wstępnie ustawionym Alpine Linux, którego można wgrać na smartfonach przeznaczonych dla Androida i na innych urządzeniach przenośnych. Na [wykazie urządzeń](https://wiki.postmarketos.org/wiki/Devices) zobaczysz postęp we wsparciu dla twojego urządzenia.

Dla urządzeń, które nie mają uprzednio zbudowanych obrazów, będziesz musiał wgrać to ręcznie przy użyciu narzędzia `pmbootstrap`. Przeczytaj szczegóły [tutaj](https://wiki.postmarketos.org/wiki/Installation_guide). Przeczytaj także stronę wiki urządzeń, aby dowiedzieć się co działa.

[Dowiedz się więcej](https://postmarketos.org)

#### Pobierz

* [Dobrze obsługiwane urządzenia](https://postmarketos.org/download/)
* [Pełna lista urządzeń](https://wiki.postmarketos.org/wiki/Devices)

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Pobierz

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, poprzednio SUSE Linux oraz SuSE Linux Professional, jest dystrybucją Linuksa sponsorowaną przez SUSE Linux GmbH i inne przedsiębiorstwa. Obecnie openSUSE dostarcza Tumbleweed na podstawie zbudowanej Przenośnej Plazmy.

#### Pobierz

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

## Urządzenia biurkowe

### postmarketOS

![](/img/pmOS.svg)

postmarketOS można uruchomić w QEMU, więc jak najbardziej nadaje się do uruchomienia Przenośnej Plazmy na twoim komputerze.

Przeczytaj o tym więcej [tutaj](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Podczas ustawiania, ustaw po prostu Przenośną Plazmę jako środowisko pulpitu.

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

### Obraz ISO amd64 oparty na Neonie

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

Ten obraz, oparty na KDE neon, można wypróbować na nie-androidowych tabletach z intelem, komputerach oraz maszynach wirtualnych.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
