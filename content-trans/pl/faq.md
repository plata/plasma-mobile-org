---
layout: page
menu:
  main:
    name: FAQ
    parent: project
    weight: 5
title: Najczęściej zadawane pytania
---
Dlaczego Przenośna Plazma nie używa Mer/Nemomobile?
-
Przenośna Plazma jest platformą oprogramowania dla urządzeń przenośnych. Nie jest to system operacyjny sam w sobie, składa się z Qt5, Szkieletów KDE, Plazmy oraz różnego oprogramowania, które jest częścią zestawu aplikacji. Przenośna Plazma może działać na dystrybucji Mer, lecz ze względu na brak czasu i zasobów, skupiamy się obecnie na KDE Neon na PinePhone jako podstawie do prób i prac rozwojowych.

Czy aplikacje z Androida będą działać na Przenośnej Plazmie?
-
Istnieją projekty takie jak [Anbox](https://anbox.io/), które stanowią Androida działającego wewnątrz kontenera Linuksa i używają jego jądra do uruchamiania programów z wydajnością niemal równą tej na systemie źródłowym. Można to wykorzystać w przyszłości, aby uruchamiać aplikacje z Androidem na systemach GNU/Linux na platformie Przenośnej Plazmy, lecz jest to złożone zadanie i na *dzisiaj*(6. września 2020) niektóre z dystrybucji już obsługują Anbox i możesz na nich uruchamiać Przenośną Plazmę.

Czy będę mógł uruchomić Przenośną Plazmę na moim urządzeniu przenośnym?
-
Obecnie, Przenośna Plazma działa na następujących urządzeniach:

* **(Zalecane) PinePhone:** Oferujemy obrazy zbudowane dla PinePhone na podstawie KDE Neon. Szczegóły nt. Przenośnej Plazmy można znaleźć w [dokumentacji](https://docs.plasma-mobile.org).
* **oparta na x86:** Jeśli chcesz wypróbować Przenośnej Plazmy na tablecie z Intelem, komputerze biurkowym/laptopie lub wirtualnej maszynie, to [obraz](https://www.plasma-mobile.org/get/) x86_64 Przenośnej Plazmy oparty na Neonie jest dla ciebie. Szczegóły nt. trwałego jej wgrania można znaleźć w [dokumentacji](https://docs.plasma-mobile.org) Przenośnej Plazmy.
* **urządzenia postmarketOS:** postmarketOS jest urządzeniem dostosowanym do dotyku z wstępnie ustawionym Alpine Linux, którego można wgrać na smartfonach przeznaczonych dla Androida i na innych urządzeniach przenośnych. Projekt ten zapewnia obsługę całkiem wielu urządzeń, a także Przenośną Plazmę jako dostępny interfejs. Znajdź swoje urządzenie w [wykazie obsługiwanych urządzeń](https://wiki.postmarketos.org/wiki/Devices) i zobacz, co działa. Później możesz przeczytać [podręcznik wgrywania pmOS](https://wiki.postmarketos.org/wiki/Installation_guide), aby go wgrać na swoim urządzeniu. Twoje odczucia mogą się różnić i **nie** jest to koniecznie reprezentatywny bieżący stan Przenośnej Plazmy.
* **Inne:** Niestety, ostatnio trzeba było porzucić obsługę urządzeń opartych na Halium (zobacz [dług techniczny](/2020/12/14/plasma-mobile-technical-debt/)). Uwzględnia to także poprzednie urządzenie odniesienia Nexus 5x.

Wgrałem Przenośną Plazmę, jakie jest hasło do zalogowania?
-
Jeśli wgrałeś Neona na swojego PinePhone poprzez skrypt wgrywając, to hasło powinno być "1234", które powinieneś móc zmienić później uruchamiając "passwd" w Konsoli. Dla Manjaro jest to 123456. Zmieniając je, pamiętaj, że na ekranie blokady możesz wprowadzać tylko cyfry.

Jeśli używasz obrazu x86, to hasło domyślnie nie jest ustawione i będziesz musiał je ustawić uruchamiając "passwd" w Konsoli, zanim będziesz w stanie, cokolwiek uwierzytelnić.

Jaki jest stan projektu?
-
Przenośna Plazma jest obecnie w trakcie wytężonych prac rozwojowych i nie jest przeznaczona do codziennego użytku. Jeśli chciałbyś współtworzyć, to [dołącz](/findyourway) do gry.
