---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Comunidade
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Grupos e canais da comunidade do Plasma Mobile:

### Canais específicos do Plasma Mobile:

* [![](/img/matrix.svg)Matrix (mais activo)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Lista de correio do Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canais do projecto relacionados com o Plasma Mobile:

* [![](/img/mail.svg)Lista de correio de desenvolvimento do Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
