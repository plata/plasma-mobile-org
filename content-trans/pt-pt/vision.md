---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Visão
    parent: project
    weight: 1
title: A Nossa Visão
---
O Plasma Mobile pretende tornar-se um sistema completo e de 'software' aberto para dispositivos móveis.<br /> Está concebido para dar aos utilizadores preocupados com a privacidade o controlo sobre as suas informações e comunicações.

O Plasma Mobile usa uma abordagem pragmática e é inclusivo para as aplicações de terceiros, permitindo ao utilizador escolher quais as aplicações e serviços a usar, oferecendo por outro lado uma experiência transparente em vários dispositivos.<br /> O Plasma Mobile implementa normas abertas e é desenvolvido segundo um processo transparente para que todos participem.
