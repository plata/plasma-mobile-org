---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Ecrã inicial do Plasma Mobile
  url: /screenshots/plasma.png
- name: KWeather, a aplicação móvel de meteorologia do Plasma
  url: /screenshots/weather.png
- name: Kalk, uma calculadora
  url: /screenshots/pp_calculator.png
- name: Megapixels, uma aplicação de câmara fotográfica
  url: /screenshots/pp_camera.png
- name: Calindori, uma aplicação de calendários
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, uma aplicação para tirar notas
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, um visualizador universal de documentos
  url: /screenshots/pp_okular01.png
- name: Angelfish, um navegador Web
  url: /screenshots/pp_angelfish.png
- name: Nota, um editor de texto
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, outro visualizador de imagens
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, um gestor de ficheiros
  url: /screenshots/pp_folders.png
- name: VVave, um leitor de música
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: O 'hardware'
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Imagens
---
As seguintes imagens foram capturadas a partir de um dispositivo Pinephone, a executar o Plasma Mobile.

{{< screenshots name="screenshots" >}}
