---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Plasma mobile startscherm
  url: /screenshots/plasma.png
- name: KWeather, weertoepassing van Plasma mobile
  url: /screenshots/weather.png
- name: Kalk, een rekenmachine-toepassing
  url: /screenshots/pp_calculator.png
- name: Megapixels, een camera-toepassing
  url: /screenshots/pp_camera.png
- name: Calindori, een agenda-toepassing
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, een toepassing voor het maken van een notitie
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, een universele documentviewer
  url: /screenshots/pp_okular01.png
- name: Angelfish, een webbrowser
  url: /screenshots/pp_angelfish.png
- name: Nota, een tekstverwerker
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, nog een afbeeldingenviewer
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, een bestandsbeheerder
  url: /screenshots/pp_folders.png
- name: VVave, een muziekspeler
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: De hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Schermafdrukken
---
De volgende schermafdrukken zijn genomen van een Pinephone apparaat waarop Plasma Mobile draait.

{{< screenshots name="screenshots" >}}
