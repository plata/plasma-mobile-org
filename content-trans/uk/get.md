---
layout: get-involved
menu:
  main:
    name: Встановити
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Дистрибутиви, які пропонують Plasma Mobile
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Мобільні пристрої

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM є дистрибутивом Manjaro, але для пристроїв на основі ARM. Його засновано на Arch Linux ARM, поєднано із інструментами Manjaro, темами та інфраструктурою для встановлення образів для вашого пристрою ARM.

[Сайт](https://manjaro.org) [Форум](https://forum.manjaro.org/c/arm/)

#### Отримання

* [Найсвіжіша стабільна (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Збірки для розробників (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Встановлення

Щодо PinePhone, ви можете знайти загальні відомості у [вікі Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) — оптимізована для сенсорних пристроїв, попередньо налаштована версія Alpine Linux, яку можна встановити на смартфони та інші мобільні пристрої. Перегляньте [список пристроїв](https://wiki.postmarketos.org/wiki/Devices), щоб ознайомитися із поступом у підтримці вашого пристрою.

Для пристроїв, для яких немає попередньо зібраних образів, вам доведеться перешити систему пристрою за допомогою програми `pmbootstrap`. Виконайте настанови, які наведено [тут](https://wiki.postmarketos.org/wiki/Installation_guide). Не забудьте також ознайомитися зі сторінкою вікі пристрою, щоб дізнатися більше про те, що саме має працювати.

[Дізнатися більше](https://postmarketos.org)

#### Отримання

* [Підтримувані пристрої](https://postmarketos.org/download/)
* [Повний список пристроїв](https://wiki.postmarketos.org/wiki/Devices)

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Отримання

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, раніше SUSE Linux і SuSE Linux Professional, — дистрибутив Linux, який спонсоровано SUSE Linux GmbH та іншими компаніями. У поточній версії openSUSE надає доступ до заснованих на Tumbleweed збірках Plasma Mobile.

#### Отримання

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

## Комп'ютерні пристрої

### postmarketOS

![](/img/pmOS.svg)

postmarketOS може працювати у QEMU, тому є придатним варіантом для того, щоб спробувати мобільну Плазму на вашому комп'ютері.

Спочатку ознайомтеся із [цією сторінкою](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). У процесі налаштовування просто виберіть як стільничне середовище мобільну Плазму.

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

### Образ ISO для amd64 на основі Neon

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

Цей образ, зібраний на основі KDE neon, можна тестувати на планшетах intel без Android, персональних комп'ютерах та віртуальних машинах.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
