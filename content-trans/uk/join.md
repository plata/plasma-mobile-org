---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Спільнота
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Групи і канали спільноти Plasma Mobile:

### Спеціалізовані канали Plasma Mobile:

* [![](/img/matrix.svg)Matrix (найактивніший канал)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Список листування Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Канали, які пов'язано із Plasma Mobile:

* [![](/img/mail.svg)Список листування розробників Плазми](https://mail.kde.org/mailman/listinfo/plasma-devel)
