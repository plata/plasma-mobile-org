---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Основний екран Плазми
  url: /screenshots/plasma.png
- name: KWeather, програма прогнозу погоди у мобільній Плазмі
  url: /screenshots/weather.png
- name: Kalk, програма-калькулятор
  url: /screenshots/pp_calculator.png
- name: Megapixels, програма для роботи з камерою
  url: /screenshots/pp_camera.png
- name: Calindori, програма-календар
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, програма для роботи із нотатками
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Мобільний Okular, універсальна програма для перегляду документів
  url: /screenshots/pp_okular01.png
- name: Angelfish, браузер
  url: /screenshots/pp_angelfish.png
- name: Nota, текстовий редактор
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, ще одна програма для перегляду зображень
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, програма для керування файлами
  url: /screenshots/pp_folders.png
- name: VVave, музичний програвач
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Обладнання
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Знімки екрана
---
Наведені нижче знімки вікон було створено на пристрої Pinephone, на якому було запущено мобільну Плазму.

{{< screenshots name="screenshots" >}}
