---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Plasma Mobile 主屏幕
  url: /screenshots/plasma.png
- name: KWeather，Plasma Mobile 的天气应用
  url: /screenshots/weather.png
- name: Kalk，计算器应用
  url: /screenshots/pp_calculator.png
- name: Megapixels，相机应用
  url: /screenshots/pp_camera.png
- name: Calindori，日历应用
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho，笔记应用
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile，通用文档查看器
  url: /screenshots/pp_okular01.png
- name: Angelfish，网页浏览器
  url: /screenshots/pp_angelfish.png
- name: Nota，文本编辑器
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix，图像查看器
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index，文件管理器
  url: /screenshots/pp_folders.png
- name: VVave，音乐播放器
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: 相关硬件
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: 截图
---
以下截图拍摄的是正在运行 Plasma Mobile 的 PinePhone 手机。

{{< screenshots name="screenshots" >}}
