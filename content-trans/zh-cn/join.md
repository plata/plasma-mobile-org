---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: 社区
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Plasma Mobile 社区工作小组和聊天频道：

### Plasma Mobile 专属频道：

* [![](/img/matrix.svg)Matrix (最为活跃)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma Mobile 邮件列表](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile 相关项目频道：

* [![](/img/mail.svg)Plasma 开发邮件列表](https://mail.kde.org/mailman/listinfo/plasma-devel)
