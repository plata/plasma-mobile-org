---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Comunità
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Gruppi e canali della comunità di Plasma Mobile:

### Canali specifici di Plasma Mobile:

* [![](/img/matrix.svg)Matrix (il più attivo)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Lista di distribuzione di Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canali di progetti relativi a Plasma Mobile:

* [![](/img/mail.svg)lista di distribuzione sullo sviluppo di Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
