---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Schermata iniziale di Plasma Mobile
  url: /screenshots/plasma.png
- name: KWeather, un'applicazione sul tempo meteorologico per Plasma mobile
  url: /screenshots/weather.png
- name: Kalk, una calcolatrice
  url: /screenshots/pp_calculator.png
- name: Megapixels, un'applicazione per fotocamera
  url: /screenshots/pp_camera.png
- name: Calindori, un'applicazione per il calendario
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, un'applicazione per prendere appunti
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular per Mobile, un visore di documenti universale
  url: /screenshots/pp_okular01.png
- name: Angelfish, un browser web
  url: /screenshots/pp_angelfish.png
- name: Nota, un editor di testi
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, un altro visore d'immagini
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, un gestore di file
  url: /screenshots/pp_folders.png
- name: VVave, un lettore musicale
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: L'hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Schermate
---
Le seguenti schermate sono state prese da un dispositivo Pinephone con in esecuzione Plasma Mobile.

{{< screenshots name="screenshots" >}}
