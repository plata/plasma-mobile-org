---
layout: get-involved
menu:
  main:
    name: Installer
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distributions proposant Plasma Mobile
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM est une distribution Manjaro mais pour périphériques ARM. Elle repose sur Arch Linux ARM, associée à des outils, des thèmes et de l'infrastructure de Manjaro pour réaliser des installations pour votre périphérique ARM.

[Site Internet](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Télécharger

* [Dernière version stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Compilations pour développeurs (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installation

Pour le PinePhone, vous pouvez trouver des informations générales sur [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) est une distribution Linux Alpine, pré configurée et optimisée pour périphériques tactiles, pouvant être installée sur les téléphones portable et d'autres périphériques mobiles. Vous pouvez afficher une [liste de périphériques](https://wiki.postmarketos.org/wiki/Devices) pour voir l'avancement de la prise en charge de votre périphérique.

Pour les appareils n'ayant pas d'images pré construites, vous devrez le flasher manuellement en utilisant l'utilitaire « pmbootstrap ». Veuillez suivre les instructions [ici] (https://wiki.postmarketos.org/wiki/Installation_guide). Veuillez vérifiez également la page wiki de l'appareil pour plus d'informations sur ce qui fonctionne.

[En savoir plus](https://postmarketos.org)

#### Télécharger

* [Périphériques largement pris en charge](https://postmarketos.org/download/)
* [Lis](h complète des périphériquesttps://wiki.postmarketos.org/wiki/Devices)

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Télécharger

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

### OpenSUSE

![](/img/openSUSE.svg)

OpenSUSE, anciennement SUSE Linux et SuSE Linux Professionnal, est un distribution Linux, développée par SUS Linux GmbH et d'autres entreprises. Actuellement, OpenSUSE fournit Tumbleweed, reposant sur les compilations de Plasma Mobile.

#### Télécharger

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

## Périphériques de bureau

### postmarketOS

![](/img/pmOS.svg)

postmarketOS est capable d'être lancé sous QEMU. Ceci est une option possible pour essayer Plasma Mobile sur votre ordinateur.

En apprendre plus sur celui-ci [ici](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Durant le processus de configuration, veuillez simplement sélectionner Plasma Mobile comme votre environnement de bureau.

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

### Image « ISO » reposant sur Neon en « amd 64 »

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

Cette image, fabriquée à partir de Neon, peut être testée sur des tablettes Intel « Non Android » et des machines virtuelles.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
