---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Écran d'accueil de Plasma Mobile
  url: /screenshots/plasma.png
- name: KWeather, une application de météo pour Plasma Mobile
  url: /screenshots/weather.png
- name: Kalk, un application de calculatrice
  url: /screenshots/pp_calculator.png
- name: Megapixels, une application de camera
  url: /screenshots/pp_camera.png
- name: Calindor, une application d'agenda
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, une application de prise de notes
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, un afficheur universel de documents
  url: /screenshots/pp_okular01.png
- name: Angelfish, un navigateur Internet
  url: /screenshots/pp_angelfish.png
- name: Nota, un éditeur de texte
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, un autre afficheur d'images
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, un gestionnaire de fichiers
  url: /screenshots/pp_folders.png
- name: VVave, un lecteur de musique
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Le matériel
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Copies d'écran
---
Les copies d'écrans ci-dessous ont été prise avec un périphérique Pinephone, fonctionnant sous Plasma Mobile.

{{< screenshots name="screenshots" >}}
