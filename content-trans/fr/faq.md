---
layout: page
menu:
  main:
    name: FAQ
    parent: project
    weight: 5
title: Foire aux questions
---
Pourquoi Plasma Mobile n'utilise t il pas Mer / Neomobile ?
-
Plasma Mobile est une plate forme logicielle pour les périphériques mobiles. Ce n'est pas un système d'exploitation, par lui même. Il regroupe Qt5, les environnements de développement de KDE, Plasma et divers logiciels faisant partie de l'ensemble des applications. Plasma Mobile peut fonctionner au dessus de la distribution Mer, mais, à cause du manque de temps et de ressources, la priorité est donnée à KDE Neon sur PinePhone, comme plate forme de tests et de développement.

Les applications Android peuvent elles fonctionner sous Plasma Mobile ?
-
Il y a des projets comme [Anbox](https://anbox.io/), faisant fonctionner Android dans un conteneur Linux et utilisant le noyau Linux pour exécuter les applications avec une performance proche des conditions natives. Ceci pourrait être mis à profit dans le futur pour faire fonctionner des applications Android au dessus d'un système GNU / Linux sous la plate forme Plasma Mobile. Mais, cette tâche parait compliquée. A * ce jour * (06 Septembre 2020), certaines distributions prennent déjà en charge Anbox et vous pouvez faire fonctionner Plasma Mobile au dessus de ces distributions.

Est ce que je peux faire fonctionner Plasma Mobile sur mon périphérique mobile ?
-
Actuellement, Plasma Mobile fonctionne sur les types suivants de périphériques :

* ** (Recommandé) PinePhone : ** des images officielles conçues pour le PinePhone au dessus de KDE Neon. Vous pouvez plus d'informations dans la [documentation](https://docs.plasma-mobile.org) de Plasma Mobile.
* ** x86-based : ** si vous voulez essayer Plasma Mobile sur un tablette Intel, ordinateur de bureau / ordinateur portable ou machine virtuelle, la version de Plasma Mobile conçue à partir de [l'image](https://www.plasma-mobile.org/get/) de Neon x86_64 est pour vous. Des informations sur comment l'installer définitivement sont disponibles dans la [documentation](https://docs.plasma-mobile.org) de Plasma Mobile.
* ** Périphériques postmarketOS : ** postmarketOS est une distribution Linux dérivée de Alpine, pouvant être installée sur des téléphones Android ou d'autres périphériques mobiles. Ce projet propose la prise en charge d'une plutôt large variété de périphériques. Il propose Plasma Mobile comme interface disponible. Veuillez chercher votre périphérique dans la [liste des périphériques pris en charge](https://wiki.postmarketos.org/wiki/Devices) et regardez ce qui fonctionne. Ensuite, vous pouvez suivre le [guide d'installation de pmOS](https://wiki.postmarketos.org/wiki/Installation_guide) pour l'installer sur votre périphérique. Son avancement pourrait varier selon le périphérique utilisé et lorsque vous utilisez un périphérique dans la catégorie en tests. Mais, il **n'est pas** nécessairement représentatif de l'état courant de Plasma Mobile.
* ** Autres : ** malheureusement, la prise en charge des périphériques reposant sur Halium a été abandonnée récemment (Veuillez consulter la (dette technique)[/2020/12/14/plasma-mobile-technical-debt.html]). Ceci concerne le périphérique précédent, référencé Nexus 5x.

J'ai installé Plasma Mobile. Quel est le mot de passe de connexion ?
-
Si vous avez installé Neon sur votre PinePhone grâce au script d'installation, le mot de passe devrait être « 1234 ». Vous pouvez alors le changer après en lançant « passwd » dans Konsole. Pour Manjaro, celui est « 123456 ». Lors de son changement, veuillez garder à l'esprit que vous ne pouvez actuellement ne saisir que des nombres sur l'écran de verrouillage.

Si vous utilisez une image « x86 », aucun mode de passe n'est défini par défaut. Et, vous aurez à le définir en lançant « passwd » dans Konsole, avant tout authentification pour n'importe quelle action.

Quel est l'avancement du projet ?
-
Plasma Mobile est actuellement en développement important et n'est pas destiné à l'utilisation quotidienne. Si vous êtes intéressé pour y contribuer, [rejoignez](/findyourway) le jeu.
