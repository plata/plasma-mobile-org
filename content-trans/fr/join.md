---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Communauté
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Groupes et canaux de la communauté de Plasma Mobile :

### Canaux spécifiques de Plasma Mobile :

* [![](/img/matrix.svg)Matrix (la plus active)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Liste de discussions de Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canaux concernant les projets relatifs à Plasma Mobile :

* [![](/img/mail.svg)Liste de discussion pour le développement de Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
