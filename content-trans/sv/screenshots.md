---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Plasma Mobil hemskärm
  url: /screenshots/plasma.png
- name: KWeather, Plasma Mobil väderprogram
  url: /screenshots/weather.png
- name: Kalk, ett miniräknarprogram
  url: /screenshots/pp_calculator.png
- name: Megapixels, ett kameraprogram
  url: /screenshots/pp_camera.png
- name: Calindori, ett kalenderprogram
  url: /screenshots/pp_calindori.png
- name: Kclock
  url: /screenshots/pp_kclock.png
- name: Buho, ett anteckningsprogram
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobil, en universell dokumentvisare
  url: /screenshots/pp_okular01.png
- name: Angelfish, en webbläsare
  url: /screenshots/pp_angelfish.png
- name: Nota, en texteditor
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, en annan bildvisare
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, en filhanterare
  url: /screenshots/pp_folders.png
- name: VVave, en musikspelare
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Maskinvaran
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Skärmbilder
---
Följande skärmbilder togs på en PinePhone som kör Plasma Mobil.

{{< screenshots name="screenshots" >}}
