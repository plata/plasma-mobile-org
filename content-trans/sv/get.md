---
layout: get-involved
menu:
  main:
    name: Installera
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distributioner som erbjuder Plasma Mobil
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobil

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM är Manjaro-distributionen, med för ARM-processorer. Den är baserad på Arch Linux ARM, kombinerat med Manjaro verktyg, teman och infrastruktur för att skapa installationsavbilder för ARM-enheter.

[Webbplats](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Nerladdning

* [Senaste stabila (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Utvecklingsbyggen (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installation

För Pinephone hittar du generell information på [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), är en pekskärmsoptimerad, förinställd Alpine Linux som kan installeras på smarta telefoner och andra mobilapparater. Titta på [apparatlistan](https://wiki.postmarketos.org/wiki/Devices) för att se status för stöd av din apparat.

För apparater som inte har förbyggda avbilder, måste du programmera det manuellt genom att använda verktyget `pmbootstrap`. Följ instruktionerna [här](https://wiki.postmarketos.org/wiki/Installation_guide). Se till att också kontrollera apparatens wiki-sida för mer information om vad som fungerar.

[Ta reda på mer](https://postmarketos.org)

#### Nerladdning

* [Apparater med bra stöd](https://postmarketos.org/download/)
* [Fullständig apparatlista](https://wiki.postmarketos.org/wiki/Devices)

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Nerladdning

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, tidigare SUSE Linux och SuSE Linux Professional, är en Linux-distribution som sponsras av SUSE Linux GmbH och andra företag. För närvarande tillhandahåller openSUSE Plasma Mobil-byggen baserade på Tumbleweed.

#### Nerladdning

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

## Skrivbordsenheter

### postmarketOS

![](/img/pmOS.svg)

postmarketOS kan köras i QEMU, och är sålunda ett lämpligt alternativ för att prova Plasma Mobil på din dator.

Läs mer om det [här](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Välj helt enkelt Plasma Mobil som skrivbordsmiljö under inställningsprocessen.

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

### Neon-baserad amd64 ISO-avbild

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

Den här avbilden, baserad på KDE Neon, kan provas på icke-Android Intel surfplattor, persondatorer och virtuella maskiner.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
