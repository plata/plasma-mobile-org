---
layout: get-involved
menu:
  main:
    name: Instal·leu-lo
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distribucions que ofereixen el Plasma Mobile
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM és la distribució Manjaro, però per a dispositius ARM. Està basada en Arch Linux ARM, combinada amb les eines, temes i infraestructura de Manjaro per a fer la instal·lació d'imatges per als dispositius ARM.

[Lloc web](https://manjaro.org) [Fòrum](https://forum.manjaro.org/c/arm/)

#### Baixada

* [Darrera estable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Construccions de desenvolupador (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instal·lació

Per al PinePhone, es pot trobar informació genèrica al [wiki del Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), és un Alpine Linux tàctil optimitzat i preconfigurat que es pot instal·lar en telèfons intel·ligents i altres dispositius mòbils. Reviseu la [llista de dispositius](https://wiki.postmarketos.org/wiki/Devices) per a veure el progrés per acceptar el vostre dispositiu.

Per a dispositius que no tenen imatges preconstruïdes, cal gravar la seva memòria flaix interna manualment amb la utilitat `pmbootstrap`. Seguiu les instruccions [aquí](https://wiki.postmarketos.org/wiki/Installation_guide). Assegureu-vos també de verificar la pàgina wiki del dispositiu per a trobar més informació quant a què funciona.

[Apreneu-ne més](https://postmarketos.org)

#### Baixada

* [Dispositius amb bona implementació](https://postmarketos.org/download/)
* [Llista completa de dispositius](https://wiki.postmarketos.org/wiki/Devices)

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Baixada

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, antigament SUSE Linux i SuSE Linux Professional, és una distribució de Linux patrocinada per SUSE Linux GmbH i altres empreses. Actualment openSUSE proporciona construccions del Plasma Mobile basades en Tumbleweed.

#### Baixada

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

## Dispositius d'escriptori

### postmarketOS

![](/img/pmOS.svg)

El postmarketOS es pot executar en el QEMU, i per tant, és una opció apropiada per a provar el Plasma Mobile en un ordinador.

Llegiu-ne més quant [aquí](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Durant el procés de configuració, només cal seleccionar el Plasma Mobile com a entorn d'escriptori.

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

### Imatge ISO amd64 basada en Neon

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

Aquesta imatge, basada en el KDE Neon, es pot provar en tauletes Intel no Android, PC i màquines virtuals.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
