---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Comunitat
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Grups i canals de la comunitat Plasma Mobile:

### Canals específics del Plasma Mobile:

* [![](/img/matrix.svg)Matrix (més actiu)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Llista de correu del Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canals de projectes relacionats amb el Plasma Mobile:

* [![](/img/mail.svg)Llista de correu de desenvolupament del Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
