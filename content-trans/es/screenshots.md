---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Pantalla inicial de Plasma Mobile
  url: /screenshots/plasma.png
- name: KWeather, la aplicación meteorológica de Plasma Mobile
  url: /screenshots/weather.png
- name: Kalk, una aplicación de la calculadora
  url: /screenshots/pp_calculator.png
- name: Megapixels, una aplicación de cámara
  url: /screenshots/pp_camera.png
- name: Calindori, una aplicación de calendario
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, una aplicación para tomar notas
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, un visor de documentos universal
  url: /screenshots/pp_okular01.png
- name: Angelfish, un navegador web
  url: /screenshots/pp_angelfish.png
- name: Nota, un editor de texto
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, otro visor de imágenes
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, un gestor de archivos
  url: /screenshots/pp_folders.png
- name: VVave, un reproductor musical
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: El hardware
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Capturas de pantalla
---
Las siguientes capturas de pantalla se han tomado en un dispositivo PinePhone que ejecuta Plasma Mobile.

{{< screenshots name="screenshots" >}}
