---
layout: page
menu:
  main:
    name: PF
    parent: project
    weight: 5
title: Preguntas frecuentes
---
¿Por qué Plasma Mobile no usa Mer/Nemomobile?
-
Plasma Mobile es una plataforma de software para dispositivos móviles. No es un sistema operativo en sí mismo: consta de Qt5, KDE Frameworks, Plasma y software diverso que es parte del conjunto de aplicaciones. Plasma Mobile puede funcionar sobre la distribución Mer, aunque debido a falta de tiempo y de recursos, en la actualidad nos hemos centrado en KDE Neon en PinePhone como base de pruebas y de desarrollo.

¿Pueden funcionar las aplicaciones de Android en Plasma Mobile?
-
Existen proyectos como [Anbox](https://anbox.io/), que es Android funcionando dentro de un contenedor Linux, y usan el *kernel* de Linux para ejecutar aplicaciones para conseguir un rendimiento cercano al nativo. Esto se podría aprovechar en el futuro para que las aplicaciones de Android se ejecuten sobre un sistema GNU/Linux con la plataforma Plasma Mobile, aunque es una tarea complicada, y a *día de hoy* (6 de septiembre de 2020) algunas distribuciones ya permiten usar Anbox para ejecutar Plasma Mobile sobre ellas.

¿Puedo ejecutar Plasma Mobile en mi dispositivo móvil?
-
En la actualidad, Plasma Mobile funciona en los siguientes tipos de dispositivos:

* **(Recomendado) PinePhone:** Ofrecemos imágenes oficiales compiladas para PinePhone sobre KDE Neon. Puede encontrar más información en la [documentación](https://docs.plasma-mobile.org) de Plasma Mobile.
* **Basado en x86:** Si quiere probar Plasma Mobile en una tableta Intel, equipo de escritorio/portátil o máquina virtual, la [imagen](https://www.plasma-mobile.org/get/) de Plasma Mobile basada en Neon para x86_64 es lo que necesita. Puede encontrar información sobre cómo instalarla de forma permanente en la [documentación](https://docs.plasma-mobile.org) de Plasma Mobile.
* **Dispositivos postmarketOS:** postmarketOS es una distribución basada en Alpine Linux que se puede instalar en teléfonos inteligentes Android y en otros dispositivos móviles. Este proyecto ofrece soporte para una gama bastante amplia de dispositivos y proporciona Plasma Mobile como una de las interfaces disponibles. Encuentre su dispositivo en la [lista de dispositivos soportados](https://wiki.postmarketos.org/wiki/Devices) y compruebe lo que funciona antes de continuar con la [guía de instalación de pmOS](https://wiki.postmarketos.org/wiki/Installation_guide) para instalarlo en su dispositivo. Su experiencia puede variar según el dispositivo que use, y si usa un dispositivo de la categoría de pruebas **no** es necesariamente representativa del estado actual de Plasma Mobile.
* **Otros:** Desafortunadamente, el soporte para dispositivos basados en Halium se ha abandonado recientemente (consulte la [deuda técnica](/2020/12/14/plasma-mobile-technical-debt/)). Esto incluye el anterior dispositivo de referencia, Nexus 5x.

He instalado Plasma Mobile. ¿Cuál es la contraseña de inicio de sesión?
-
Si ha instalado Neon en su PinePhone usando el guion de instalación, la contraseña debe ser «1234», aunque puede cambiarla posteriormente ejecutando «passwd» en Konsole. Para Manjaro es «123456». Cuando la cambie, recuerde que solo se pueden introducir números en la pantalla de bloqueo.

Si está usando la imagen para x86, no se define ninguna contraseña de forma predeterminada, por lo que tendrá que definir una ejecutando «passwd» en Konsole antes de poder autenticarse para cualquier cosa.

¿Cuál es el estado del proyecto?
-
Plasma Mobile está en la actualidad en intenso desarrollo y no está pensado para que lo use como motor diario. Si está interesado en colaborar, [únase](/findyourway) al juego.
