---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: Comunidad
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Grupos y canales de la comunidad de Plasma Mobile:

### Canales específicos de Plasma Mobile:

* [![](/img/matrix.svg)Matrix (más activo)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)
* [![](/img/mail.svg)Lista de distribución de Plasma Mobile](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Canales de proyectos relacionados con Plasma Mobile:

* [![](/img/mail.svg)Lista de distribución del desarrollo de Plasma](https://mail.kde.org/mailman/listinfo/plasma-devel)
