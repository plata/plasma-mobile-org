---
layout: page
menu:
  main:
    name: MEG
    parent: project
    weight: 5
title: Galdera ohikoenak
---
Zerbatik ez du Plasma Mobilek Mer/Nemomobile erabiltzen?
-
Plasma Mobile gailu mugikorretarako software plataforma bat da. Ez da sistema eragile bat, Qt5, KDE azpiegitura («Frameworks»), Plasma eta aplikazio multzoaren zati diren hainbat aplikaziok osatzen dute. Plasma Mobile ibil daiteke Mer banaketaren gainean, baina denbora eta baliabide faltagatik, probatarako eta garapenerako oinarri gisa, gure arreta, egun, KDE Neon-en PinePhone-an jartzen ari gara.

Android aplikazioak Plasma Mobilen ibil daitezke?
-
[Anbox](https://anbox.io/) gisako proiektuak daude, Android Linux edukitzaile batean ibiltzeko, eta Linux muina erabiltzen duena aplikazioak ia bertakoak balira bezalako errendimendua lortzeko. Hori etorkizunean aprobetxa liteke GNU/Linux sistemako Plasma Mobile plataformetan Android aplikazioak exekutatzeko, baina zeregin korapilatsua da, eta *gaur* egun (2020ko irailak 6) banaketa batzuek Anbox onartzen dute eta banaketa horietan Plasma Mobile ibil dezakezu.

Ibil dezaket Plasma Mobile nire gailu mugikorrean?
-
Gaur egun, Plasma Mobile ondoko gailuetan dabil:

* **(Gomendatua) PinePhone:** KDE Neon-en oinarrituta, PinPhone-rako eraikitako irudi ofizialak eskaintzen ditugu. Plasma Mobile-ren [dokumentazioan](https://docs.plasma-mobile.org) informazio gehiago aurki dezakezu.
* **x86-oinarrikoa:** Plasma Mobile, Intel tableta batean, mahaigainekoan/magalekoan, edo alegiazko makina batean probatu nahi baduzu,  Neon-oinarriko x86_64 Plasma Mobile [irudia](https://www.plasma-mobile.org/get/) zuretzako da. Betiko instalatzeko moduari buruzko informazioa Plama Mobile-ren [dokumentazioan](https://docs.plasma-mobile.org) aurki daiteke.
* **postmarketOS gailuak:** postmarketOS, Android telefono adimendunetan eta beste gailu mugikorretan instala daitekeen, Alpine Linux-en oinarritutako banaketa bat da. Proiektu honek gailu aukera zabal baterako euskarria eskaintzen du, eta Plasma Mobile  interfaze erabilgarri gisa eskaintzen du. Mesedez, bilatu zure gailua [gailu bateragarrien zerrenda](https://wiki.postmarketos.org/wiki/Devices) eta ikusi zer dabilen, ondoren [pmOS instalatzeko gida](https://wiki.postmarketos.org/wiki/Installation_guide) erabil dezakezu zure gailuan instalatzeko. Erabiltzen duzun gailuaren arabera zure esperientzia alda daiteke eta probako kategorian dagoen gailu bat erabiltzean **ez** du zertan Plasma Mobile-n egungo egoeraren adierazgarri izan behar.
* **Bestelakoak:** Zoritxarrez, Halium oinarriko gailuen euskarria duela gutxi jaregin behar izan da (begiratu [zor teknikoak](/2020/12/14/plasma-mobile-technical-debt/)). Horrek barnean hartzen du aurretik erreferentzia zen Nexus 5.x gailua.

Plasma Mobile instalatu dut, zein da saio-hasteko pasahitza?
-
Zure PinePhone-an, Neon, instalatzeko gidoiaren bidez instalatu baduzu, pasahitza «1234» izan beharko litzateke, eta Konsole-n «passwd» exekutatuz alda dezakezu. Manjaro-n «123456» da. Hura aldatzean, gogoan izan giltzatze-pantailan zenbakiak baino ezin dituzula sartu.

x86 irudia erabiltzen ari bazara, ez da ezartzen pasahitz lehenetsirik, eta hura ezarri beharko duzu, Konsole-n «passwd» exekutatuz, ezertarako autentifikatzeko gai izateko.

Zein da proiektuaren egoera?
-
Plasma Mobile, gaur egun, garapen sakonean dago eta ez dago eguneroko erabilera arrunterako pentsatua. Lagundu nahi baduzu, [elkartu](/findyourway) jokora.
