---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Plasma Mobile etxeko-pantaila
  url: /screenshots/plasma.png
- name: KWeather, Plasma Mobile eguraldi aplikazioa
  url: /screenshots/weather.png
- name: Kalk, kalkulagailu aplikazio bat
  url: /screenshots/pp_calculator.png
- name: Megapixels, kamera aplikazio bat
  url: /screenshots/pp_camera.png
- name: Calindori, egutegi aplikazio bat
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, oharrak hartzeko aplikazio bat
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, dokumentu erakusle unibertsal bat
  url: /screenshots/pp_okular01.png
- name: Angelfish, web arakatzaile bat
  url: /screenshots/pp_angelfish.png
- name: Nota, testu editore bat
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, beste irudi erakusle bat
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, fitxategi-kudeatzaile bat
  url: /screenshots/pp_folders.png
- name: VVave, musika jotzaile bat
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Hardwarea
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Pantaila-argazkiak
---
Ondok pantaila-argazkiak Plasma Mobile ibiltzen zuen Pinephone batetik ateratakoak dira.

{{< screenshots name="screenshots" >}}
