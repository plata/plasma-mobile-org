---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/join.scss
title: 커뮤니티
---
If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)!

<img src="/img/konqi/konqi-contribute.png" width=200px/>

Plasma 모바일 커뮤니티 그룹과 채널:

### Plasma 모바일 전용 채널:

* [![](/img/matrix.svg)Matrix(가장 활성화됨)](https://matrix.to/#/#plasmamobile:matrix.org)
* [![](/img/telegram.svg)텔레그램](https://t.me/plasmamobile)
* [![](/img/mail.svg)Plasma 모바일 메일링 리스트](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma 모바일 관련 프로젝트 채널:

* [![](/img/mail.svg)Plasma 개발 메일링 리스트](https://mail.kde.org/mailman/listinfo/plasma-devel)
