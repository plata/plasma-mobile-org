---
layout: page
menu:
  main:
    name: 자주 묻는 질문
    parent: project
    weight: 5
title: 자주 묻는 질문
---
왜 Plasma 모바일은 Mer/Nemomobile을 사용하지 않나요?
-
Plasma 모바일은 모바일 장치용 소프트웨어 플랫폼입니다. 그 자체적으로 운영 체제를 구성하지 않으며, Qt5, KDE 프레임워크, Plasma 및 기타 프로그램으로 구성되어 있습니다. Plasma 모바일은 Mer 배포판에서 작동이 가능하지만 시간과 자원의 부족으로 인하여 PinePhone 상의 KDE Neon에서 개발과 시험을 진행하고 있습니다.

Plasma 모바일에서 안드로이드 앱을 사용할 수 있나요?
-
리눅스 컨테이너에서 안드로이드를 실행시킬 수 있는 [Anbox](https://anbox.io)가 있습니다. 리눅스 커널을 사용하므로 프로그램을 네이티브 성능에 가깝게 구동할 수 있습니다. 차후에 GNU/리눅스 시스템 및 Plasma 모바일 플랫폼에서 안드로이드 앱을 실행시키는 데 이 프로젝트의 자원을 활용할 수는 있으나, 이 작업은 복잡합니다. 2020년 9월 6일 기준 일부 배포판에서는 Anbox를 지원하며 해당 배포판에서 Plasma 모바일을 구동할 수 있습니다.

내 장치에서 Plasma 모바일을 사용할 수 있나요?
-
현재 다음 장치에서 Plasma 모바일을 사용할 수 있습니다:

* **(추천)PinePhone:** KDE Neon 기반 PinePhone용 공식 이미지를 제공합니다. Plasma 모바일 [문서](https://docs.plasma-mobile.org)에서 자세히 알아 보십시오.
* **x86 기반:** Plasma 모바일을 인텔/AMD 태블릿, 데스크톱, 노트북에서 시험해 보려면 x86_64 Neon 기반 Plasma 모바일 [이미지](https://www.plasma-mobile.org/get/)를 사용하십시오. 장치에 설치할 수 있는 정보는 Plasma 모바일 [문서](https://docs.plasma-mobile.org)를 참조하십시오.
* **postmarketOS 장치:** postmarketOS는 안드로이드 스마트폰과 기타 모바일 장치에 설치할 수 있는 Alpine 리눅스 기반 배포판입니다. 이 프로젝트는 Plasma 모바일을 사용자 인터페이스로 지원합니다. postmarketOS가 [지원하는 장치 목록](https://wiki.postmarketos.org/wiki/Devices)에서 사용 중인 장치 지원 여부와 작동하는 구성 요소를 확인했다면 [pmOS 설치 가이드](https://wiki.postmarketos.org/wiki/Installation_guide)를 참조하여 장치에 설치하십시오. 작동 여부는 장치에 따라 다르며, 테스트 대상 장치가 아니라면 Plasma 모바일의 현재 상태를 정확하게 반영하지 **못할** 수도 있습니다.
* **기타:** Halium 기반 장치 지원은 최근에 삭제되었습니다. 자세한 정보는 [기술 부채](/2020/12/14/plasma-mobile-technical-debt)를 참조하십시오. 이전의 레퍼런스 장치였던 Nexus 5X도 해당합니다.

Plasma 모바일을 설치했는데 로그인 암호가 무엇인가요?
-
설치 스크립트로 PinePhone에 Neon을 설치했다면 암호는 "1234"입니다. Konsole에서 "passwd" 명령을 실행하여 바꿀 수 있습니다. Manjaro의 경우에는 "123456"입니다. 암호를 변경할 때에는 현재 잠금 화면에서 숫자만 입력할 수 있음을 유의하십시오.

x86 이미지를 사용한다면 암호가 설정되어 있지 않으며, 인증해야 하는 명령어를 실행하기 전에 Konsole에서 "passwd" 명령을 실행하여 암호를 설정해야 합니다.

현재 프로젝트 상태는 어떤가요?
-
Plasma 모바일은 현재 활발하게 개발 중이며 일상 사용 용도로는 적합하지 않습니다. 기여에 관심이 있으시다면 [역할](/findyourway)을 알아 보십시오.
