---
cssFiles:
- css/swiper-bundle.min.css
jsFiles:
- js/swiper-bundle.min.js
- js/swiper-init.js
layout: page
menu:
  main:
    parent: project
    weight: 2
screenshots:
- name: Domači zaslon Plasma mobile
  url: /screenshots/plasma.png
- name: KWeather, Plasma mobile vremenska aplikacija
  url: /screenshots/weather.png
- name: Kalk, aplikacija kalkulatorja
  url: /screenshots/pp_calculator.png
- name: Megapixels, aplikacija kamere
  url: /screenshots/pp_camera.png
- name: Calindori, aplikacija koledarja
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, aplikacija zapiskov
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, vpogledovalnik dokumentov
  url: /screenshots/pp_okular01.png
- name: Angelfish, spletni brskalnik
  url: /screenshots/pp_angelfish.png
- name: Nota, urejevalnik besedil
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, ogledovalnik slik
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, upravljalnik datotek
  url: /screenshots/pp_folders.png
- name: VVave, predvajalnik glasbe
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: Strojna oprema
  url: /screenshots/20201110_092718.jpg
scssFiles:
- scss/components/swiper.scss
title: Zaslonske slike
---
Naslednji posnetki zaslona so bili posneti iz naprave Pinephone, ki poganja Plasma Mobile.

{{< screenshots name="screenshots" >}}
