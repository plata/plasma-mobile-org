---
layout: get-involved
menu:
  main:
    name: Namesti
    weight: 4
no_text: true
scssFiles:
- scss/get.scss
title: Distribucije, ki nudijo Plasma Mobile
---
Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobilci

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM je distribucija Manjaro za ARM naprave. Temelji na Arch Linux ARM v kombinaciji z orodji, temami in infrastrukturo Manjaro za izdelavo namestitvenih datotek za vašo napravo ARM.

[Spletišče](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Prenos

* [Zadnji stabilni (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Razvojne verzije (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Namestitev

Za PinePhone lahko najdete generične informacije na [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) je vnaprej sestavljen Alpine Linux, optimiziran za rabo z dotikanjem, ki ga je mogoče namestiti na pametne telefone in druge mobilne naprave. Poglejte [seznam naprav](https://wiki.postmarketos.org/wiki/Devices) za ogled napredka podpore vaše naprave.

Za naprave, ki nimajo vnaprej izdelanih namestitvenih slik, jih boste morali napisati ročno s pripomočkom `pmbootstrap`. Sledite navodilom [tukaj](https://wiki.postmarketos.org/wiki/Installation_guide). Preverite tudiwiki strani naprave za več informacij o tem, kaj od programja deluje.

[Learn more](https://postmarketos.org)

#### Prenos

* [Dobro podprte naprave](https://postmarketos.org/download/)
* [Celotni seznam naprav](https://wiki.postmarketos.org/wiki/Devices)

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Prenos

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

### openSUSE

![](/img/openSUSE.svg)

openSUSE, prej SUSE Linux in SuSE Linux Professional, je distribucija Linuxa, ki jo sponzorira SUSE Linux GmbH in druga podjetja. Trenutno openSUSE ponuja sestave Plasma Mobile na osnovi sistema Tumbleweed.

#### Prenos

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

## Namizne naprave

### postmarketOS

![](/img/pmOS.svg)

postmarketOS je mogoče poganjati v QEMU in tako je primerna možnost za poskus rabe Plasma Mobile na vašem računalniku.

Preberite si več o tem [tukaj](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). Med postopkom nameščanja enostavno izberite Plasma Mobile kot namizno okolje.

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

### ISO datoteka na osnovi Neona za amd64

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

To datoteko ISO, ki temelji na KDE Neonu lahko preizkusite na ne-androidnih tabličnih računalnikih Intel, osebnih računalnikih in navideznih računalnikih.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
